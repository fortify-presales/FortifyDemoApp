import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

	private static final Logger logger = Logger.getLogger(String.valueOf(Main.class));

	public static void main(String[] args){

		String filename = args[0];
		logger.log(Level.ALL, "Hello, World!");
		Thing someThing = new Thing(1, "flippity", "a description of a flippity");
		logger.log(Level.ALL, someThing.toString());
		Utils.SerializeToFile(someThing, filename);
		Thing sameThing = (Thing) Utils.DeserializeFromFile(filename);
		logger.log(Level.ALL, sameThing.toString());
	}
}
